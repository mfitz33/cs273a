# README #

### Predicting the Clinical Outcome of Papillary Thyroid Carcinoma Patients through Publicly Available Omics Data ###

* Quick summary:
This is a data analysis and visualization interface of papillary thyroid carcinoma omics analysis.

* Version: 
1.0.0

* Dependencies: 
Ruby 2.0.0 and Rails 4.0.0

* Contact: 
Michael Ryan Fitzpatrick <mfitz3 at stanford.edu>; 
Kun-Hsing Yu <khyu at stanford.edu>; 
Luke Pappas <lpappas9 at stanford.edu>; 
Jessica Kung <jzkung at stanford.edu>